# generated by cabal-rpm-0.12.1
# https://fedoraproject.org/wiki/Packaging:Haskell

%global pkg_name http-client
%global pkgver %{pkg_name}-%{version}

%bcond_with tests

Name:           ghc-%{pkg_name}
Version:        0.5.7.1
Release:        2%{?dist}
Summary:        A basic HTTP client engine

License:        MIT
Url:            https://hackage.haskell.org/package/%{pkg_name}
Source0:        https://hackage.haskell.org/package/%{pkgver}/%{pkgver}.tar.gz

BuildRequires:  ghc-Cabal-devel
BuildRequires:  ghc-rpm-macros
# Begin cabal-rpm deps:
BuildRequires:  ghc-array-devel
BuildRequires:  ghc-base64-bytestring-devel
BuildRequires:  ghc-blaze-builder-devel
BuildRequires:  ghc-bytestring-devel
BuildRequires:  ghc-case-insensitive-devel
BuildRequires:  ghc-containers-devel
BuildRequires:  ghc-cookie-devel
BuildRequires:  ghc-deepseq-devel
BuildRequires:  ghc-exceptions-devel
BuildRequires:  ghc-filepath-devel
BuildRequires:  ghc-http-types-devel
BuildRequires:  ghc-mime-types-devel
BuildRequires:  ghc-network-devel
BuildRequires:  ghc-network-uri-devel
BuildRequires:  ghc-random-devel
BuildRequires:  ghc-streaming-commons-devel
BuildRequires:  ghc-text-devel
BuildRequires:  ghc-time-devel
BuildRequires:  ghc-transformers-devel
%if %{with tests}
BuildRequires:  ghc-async-devel
BuildRequires:  ghc-directory-devel
BuildRequires:  ghc-hspec-devel
BuildRequires:  ghc-monad-control-devel
BuildRequires:  ghc-zlib-devel
%endif
# End cabal-rpm deps

%description
An HTTP client engine, intended as a base layer for more user-friendly
packages, refactored from http-conduit. Note that, if you want to make HTTPS
secure connections, you should use http-client-tls in addition to this library.


%package devel
Summary:        Haskell %{pkg_name} library development files
Provides:       %{name}-static = %{version}-%{release}
Provides:       %{name}-doc = %{version}-%{release}
%if %{defined ghc_version}
Requires:       ghc-compiler = %{ghc_version}
Requires(post): ghc-compiler = %{ghc_version}
Requires(postun): ghc-compiler = %{ghc_version}
%endif
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
This package provides the Haskell %{pkg_name} library development files.


%package devel-doc
Summary:        Haskell %{pkg_name} library development documentation

BuildArch:      noarch

%description devel-doc
This package provides the Haskell %{pkg_name} library development
documentation.


%prep
%setup -q -n %{pkgver}


%build
%ghc_lib_build


%install
%ghc_lib_install
grep -v "%{_docdir}/ghc/html/libraries/%{pkgver}" %{name}-devel.files > %{name}-devel-nodoc.files
grep "%{_docdir}/ghc/html/libraries/%{pkgver}" %{name}-devel.files > %{name}-devel-doc.files


%check
%cabal_test


%post devel
%ghc_pkg_recache


%postun devel
%ghc_pkg_recache


%files -f %{name}.files
%license LICENSE


%files devel -f %{name}-devel-nodoc.files
%doc ChangeLog.md README.md


%files devel-doc -f %{name}-devel-doc.files
%doc ChangeLog.md README.md


%changelog
* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.7.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Wed Jan 24 2018 Jens Petersen <petersen@redhat.com> - 0.5.7.1-1
- update to 0.5.7.1

* Tue Aug 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.5.7.0-2
- Split docs into separate subpackage.
- Fix summary and description.
- Update to latest spec template.

* Sat Jul 22 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.5.7.0-1
- Update to latest version.

* Mon Jul 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.4.28-8
- Move things around based on their need.

* Sat Dec 17 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.4.28-7
- Bump to rebuild against new dependencies

* Fri Dec 16 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.4.28-6
- Bump to rebuild against new dependencies

* Thu Dec 15 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.4.28-5
- Update release to be newer than previous builds

* Thu Dec 15 2016 Fedora Haskell SIG <haskell@lists.fedoraproject.org> - 0.4.28-1
- spec file generated by cabal-rpm-0.10.0
